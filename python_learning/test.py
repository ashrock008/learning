a=[1,2,3,4,5]
a.append("a")
print(a)
a.copy()
print(a)


a=1
b=a
a=2
print(b)

from copy import deepcopy,copy

a=[[1,2,3],1,2,3]
b=deepcopy(a)
a[0][0]=2
print(b)

a={"o":[1,2,3],2:2}
b=deepcopy(a)
# b=a
a["o"][0]=3
print(b)

a=[1,2,3,4,5]
print(a)
a.clear
print(a)

list2 = ['2', '1', '2', '1', '2', '1', '2', '1']
print(list2.count('2'))

a=[1,2,3,4,5]
a.extend([2,3])
print(a)

a=[1,2,3,3,3,5,4,3,4,5,4,5]
print(a.index(4))

a=[1,2,3,4,5]
a.insert(1,2)
print(a)


a = [1, 2, 3, 4, 5]
b=a.pop(2)
print(b)

a = [1, 2, 3, 4, 5]
a.remove(2)
print(a)


a = [1, 2, 3, 4, 5]
a.reverse()
print(a)

a = [9,3,5,7,4,64,2,1]
a.sort()
print(a)

a = [9,3,5,7,4,64,2,1]
a.sort(reverse = True)
print(a)

#dictionary

d = {
  "a": ["0"],
 

}
print(d)
print (type(d["a"]))
print (type("0"))
print (type(False))
print (type("False"))
print("*"*50)
print(len(d))
d["a"].append(30)
print(d["a"])


d =  {
  "a": ["0"],
}
d.clear
print(d)

D = {}
print("Empty Dictionary: ")
print(D)

d = {"a": "ashwin", "b": "monke", "c":"doge"}
print("a :", d["a"])
print("b : ", d["b"])
print("location : ", d["c"])

d = {'tesla':356,'Ferrari':100,'mclaren':156,'lambo':200}

result=len(d.keys())
result2=len(d.values())
print("Count dictionary words keys:",result)
print("Count dictionary words values:",result2)


D = {1: 'welcome', 2: 'to',
        3: {'A': 'build', 'dubai': 'To', 'Canada': 'morning'}}
  
print(D)

a= {1: 'welcome', 2: 'to', 3: {'A': 'build', 'dubai': 'To', 'Canada': 'morning'}}
b= {1: 'ws', 1: 'o', 2: {'v': 'bld0', 'du': 'T', 'Caa': 'mng'}}
b = a.copy()
print(a)

d = {'tesla':356,'Ferrari':100,'mclaren':156,'lambo':200}
d.clear()
print(d)

d = {'tesla':356,'Ferrari':100,'mclaren':156,'lambo':200}
print(d.get('tesla'))

d = {'tesla':356,'Ferrari':100,'mclaren':156,'lambo':200}
print(d.items())

d = {'tesla':356,'Ferrari':100,'mclaren':156,'lambo':200}
print(d.keys())

d = {'tesla':356,'Ferrari':100,'mclaren':156,'lambo':200}
c = {'roma':200,'nevera':300}
c.pop('roma')
print(c)

d = {'tesla':356,'Ferrari':100,'mclaren':156,'lambo':200}
c = {'roma':200,'nevera':300}
c.popitem()
print(c)

d = {'tesla':356,'Ferrari':100,'mclaren':156,'lambo':200}
c = {'roma':200,'nevera':300}
d.update(c)
d["tesla"]=400
d["ashwin"]=1000
print(d)

d = {'tesla':356,'Ferrari':100,'mclaren':156,'lambo':200}
print(d.values())
# jfsdndj

# set add

ash = {'a', 's', 'k'}
ash.add('m')
print(ash)

ash = {'a', 's', 'k'}
ash.remove('a')
print(ash)

ash = {'a', 's', 'k'}
ash.clear()
print('cleared values')

ash = {'a', 's', 'k'}
ash.copy()
print(ash)

ash = {'a', 's', 'k'}
ash.pop()
print(ash)


ash = {'a', 's', 'k'}
monke = {'c','b','m'}
donke = {'a','j'}
set1 = set(monke)
set2 = set(ash)
set1.update(donke)
print(set1)

ash = {'a', 's', 'k'}
monke = {'c','b','m'}
 
print("A U B:", ash.union(monke))



c = {10, 40, 30, 50, 90}
b = {20, 30, 90, 40, 60}
print (c.difference(b))
print (b.difference(c))